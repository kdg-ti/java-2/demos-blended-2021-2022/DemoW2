package model;

/**
 * Mark Goovaerts
 * 27/09/2021
 */
public class Cat extends Animal {
    public Cat(String name) {
        super(name);
    }

    @Override
    public String speak() {
        return "Meaw!";
    }
}
