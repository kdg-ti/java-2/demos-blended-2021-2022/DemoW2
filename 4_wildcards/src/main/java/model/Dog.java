package model;

/**
 * Mark Goovaerts
 * 27/09/2021
 */
public class Dog extends Animal {
    public Dog(String name){
        super(name);
    }

    @Override
    public String speak() {
        return "Woof!";
    }
}
