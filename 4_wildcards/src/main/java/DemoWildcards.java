import model.Animal;
import model.Cat;
import model.Dog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Demo voor het gebruik van wildcards in combinatie met generics
 *
 * Mark Goovaerts
 * 27/09/2021
 */
public class DemoWildcards {
    public static void main(String[] args) {
        List<Dog> myDogs = Arrays.asList(
                new Dog("Spike"),
                new Dog("Pluto"));
        List<Cat> myCats = Arrays.asList(
                new Cat("Garfield"));

        System.out.println("OK");
    }
}
