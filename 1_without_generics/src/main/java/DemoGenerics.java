import java.util.ArrayList;
import java.util.List;

/**
 * Mark Goovaerts
 * 27/09/2021
 */
public class DemoGenerics {
    public static void main(String[] args) {
        List myList = new ArrayList();
        myList.add("test");
        myList.add(4);
        System.out.println(myList);

    }
}
